import numpy as np
import cv2
cap = cv2.VideoCapture("/home/datamarindo/Downloads/fish.mp4")
frame_width = int(cap.get(cv2.CAP_PROP_FRAME_WIDTH) + 0.5)
frame_height = int(cap.get(cv2.CAP_PROP_FRAME_HEIGHT) + 0.5)
size = (frame_width, frame_height)
   
result = cv2.VideoWriter('filename.avi',
cv2.VideoWriter_fourcc(*'MJPG'),20.0, size, 0) # el 0 hizo que funcionara exportar a b&n

while(cap.isOpened()):
    ret, frame = cap.read()
    if ret==True:
        frame =  cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)                
        ret, threshed = cv2.threshold(frame,80,125,cv2.THRESH_BINARY)
        newFrame = np.array(threshed)
        result.write(newFrame) 
        cv2.imshow('PECESITOS',newFrame)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            break
    else:
        break

# Release everything if job is finished

cap.release()
result.release()
cv2.destroyAllWindows()

