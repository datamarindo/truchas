# Descripción

Este es un repositorio de scripts de opencv para un estudio de la respuesta de comportamiento de las truchas en un estanque ante la operación de turbinas

## materiales

Se usan videos de una go-pro sumergida en el agua, en nadir y horizontales

## proceso

se probarán varias aproximaciones:

* Flujo óptico (Lucas Kanade)
* Haar cascades
* Binarizar (threshold) las imágenes y obtener estadísticas de tendencia central/dispersión por pixel